# NOTES_JS Application

Restful CRUD API webservice application using Node.js, Express and MongoDB for 
managing and sorting in database simple notes (without the UI part). 

## Setup

1. Apllication required below software to be install on PC to work:

- MonogoDB with defaul port: ```localhost:27017```

Link: <https://www.mongodb.com/download-center/community>

2. Install dependencies

```bash
npm install
```

3. Run Server under the below command:

```bash
npm run dev
```

4. Use Postman collection for testing and checking all endpoint : 

```
NOTES_JS.postman_collection.json
```

You can browse API at <http://localhost:5000>

### List of all endpoint:

* MAIN PAGE: <http://127.0.0.1:5000/>

* ADD NEW NOTE: <http://127.0.0.1:5000/notes> 
    
    Required JSON:

    ```javascript
    {
	"title" : "Test Title ",
	"content" : "Test Content "
    } 
    ```
    
    RESPONSE JSON example:

    ```javascript
    {
    "deleted": false,
    "_id": "5ebaa718db80f1206c9240d1",
    "title": "Test Title ",
    "content": "Test Content ",
    "versionIndex": 0,
    "created": "2020-05-12T13:39:36.899Z",
    "modified": "2020-05-12T13:39:36.899Z",
    "__v": 0
    }
    ```

* GET ALL NOTES WITHOUT DELETED: <http://127.0.0.1:5000/notes>

* GET NOTE BY ID: <http://127.0.0.1:5000/notes/id>

    RESPONSE JSON example:

    ```javascript
    {
    "deleted": false,
    "_id": "5ebaa718db80f1206c9240d1",
    "title": "Test Title ",
    "content": "Test Content ",
    "versionIndex": 0,
    "created": "2020-05-12T13:39:36.899Z",
    "modified": "2020-05-12T13:39:36.899Z",
    "__v": 0
    }
    ```

* UPDATE NOTE: <http://127.0.0.1:5000/notes/id>

    Required JSON:

    ```javascript
    {
	"title" : "Test Title 1",
	"content" : "Test Content 1"
    } 
    ```
    
    RESPONSE JSON example:

    ```javascript
    {
    "deleted": false,
    "_id": "5ebaa718db80f1206c9240d1",
    "title": "Test Title 1",
    "content": "Test Content 1",
    "versionIndex": 1,
    "created": "2020-05-12T13:39:36.899Z",
    "modified": "2020-05-12T13:39:36.899Z",
    "__v": 0
    }
    ```

* REMOVE NOTE (UPDATE DELETED: TRUE): <http://127.0.0.1:5000/notes/id>

* SHOW FULL HISTORY OF NOTES: <http://127.0.0.1:5000/notes_full_history/>

* GET NOTE HISTORY BY ID: <http://127.0.0.1:5000/note_history/id>

## Testing

To run tests use command:

```bash
npm test
```




