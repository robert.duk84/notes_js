const express = require('express');
const bodyParser = require('body-parser');

// create express
const app = express();
const port = 5000;

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

//database setup config
const dbSetup = require('./config/database.setup.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.set('useFindAndModify', false);

/**
 * DURING TEST DISABLE/COMMENT BELOW DATABASE CONNECTION CHECK
 */
//database connection check
mongoose.connect(dbSetup.url, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
}).then(() => {
    console.log("Connected to the NOTES_JS database!");
}).catch(err => {
    console.log('Connection fail to the NOTES_JS database.', err);
    process.exit();
});

//  route 
app.get('/', (req, res) => {
    res.json({"message": "Welcom to NOTES_JS application. "});
});

// Notes routes
require('./app/routes/note.routes.js') (app);

// request check
app.listen(port, () => {
    console.log(`Server is listening on port: ${port}`);
});