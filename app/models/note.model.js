const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
        },
    content: {
        type: String,
        required: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    versionOf: String,
    versionIndex: Number,
    },
    {timestamps: { createdAt: 'created', updatedAt: 'modified'}})

module.exports = mongoose.model('Note', noteSchema);

