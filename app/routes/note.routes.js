module.exports = (app) => {
    const notes = require('../controllers/note.controller.js');

    // Create new note in Notes
    app.post('/notes', notes.create);

    // Show all notes from Notes without deleted
    app.get('/notes', notes.getAll);

    // Show single note from Notes without deleted
    app.get('/notes/:noteId', notes.getById)

    // Update correct note by noteId
    app.put('/notes/:noteId', notes.updateById);

    // Delete correct note by noteId
    app.delete('/notes/:noteId', notes.deleteById);

    // Show full history of changes for particulary note by noteId
    app.get('/note_history/:noteId', notes.getHistory);

    // Show all notes full history in Notes with deleted.
    app.get('/notes_full_history/', notes.fullHistory);
}