const Note = require('../models/note.model.js');

// Create and Save new note in Notes
exports.create = (req, res) => {  
    const note = new Note({
        title: req.body.title,
        content: req.body.content,
        versionIndex: req.body.versionIndex || 0,
        versionOf: req.body.versionOf
    });
    note.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Error occurred during creating the Note."
            });
        });
};

// Show all notes from the database without Deleted
exports.getAll = (req, res) => {
    Note.find({
        deleted: { $eq: false},
        versionOf: {$eq: null}
    })
    .then(notes => {
        let notesIds = notes.map(function (note) { return note._id; });
        Note.find({versionOf: {$in: notesIds}})
            .sort({versionIndex:"desc"})
            .then(notesVersions => {
                let latestNotes = [];
                notes.forEach(orgNote => {
                    const versionsFound = notesVersions.filter(function(noteVersion){
                        return noteVersion.versionOf == orgNote._id.toString();
                    });
                    if(versionsFound && versionsFound[0]){
                        if(!versionsFound[0].deleted){
                            latestNotes.push(versionsFound[0]);
                        }
                    } else {
                        latestNotes.push(orgNote);
                    }
                });
                res.send(latestNotes);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Error occurred during retrieving notes."
                });
            });
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Error occurred during retrieving notes."
        });
    });
};

// Show single note from the database
exports.getById = (req, res) => {
    Note.find({ deleted: {$eq: false}})      
        .or([{_id:req.params.noteId},{versionOf:req.params.noteId}])
        .sort({ versionIndex: 'desc'})
        .limit(1)
        .then(note => {
            if(note && note[0]) {
                res.send(note[0]);
            } else {
                res.status(500).send("Error retrieving note with id : " + req.params.noteId + ", item was deleted");
            }
        })
        .catch(err => {
            if(err.kind ==='ObjectId') {
                res.status(404).send("Note not found with id " + req.params.noteId);
            }
            res.status(400).send("Note not found with id " + req.params.noteId);
        });
};

// Update single note from database by noteId
exports.updateById = (req, res) => {
    Note.find()
    .setOptions({deleted : false })
    .or([{_id : req.params.noteId},{versionOf : req.params.noteId}])
    .sort({ versionIndex: 'desc'})
    .limit(1)
    .then(note => {
        if(note && note[0]){
            const newNote = new Note({
                title: req.body.title,
                content: req.body.content,
                versionIndex: note[0].versionIndex + 1
            });
            if(note[0].versionOf){
                newNote.versionOf = note[0].versionOf;
            } else {
                newNote.versionOf = req.params.noteId;
            }
            newNote.save()
            .then(savedNote => {
                res.send(savedNote);
            }).catch(err => {
                return res.status(500).send({
                    message: err.message || "Error occurred during retrieving notes."
                });
            });
        } else{
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
    }).catch(err => {
        if(err.kind ==='ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        return res.status(400).send({
            message: "Note not found with id " + req.params.noteId
        });
    });
};

// Delete single note from database identified by noteId 
exports.deleteById = (req, res) => {
    Note.findByIdAndUpdate(req.params.noteId, {
        $set: { "deleted" : true }
    })
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        res.send({message: "Note was deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        return res.status(500).send({
            message: "Could not deleted note with id " + req.paras.noteId
        });
    });
};

// Show single note full history in application
exports.getHistory = (req, res) => {
    Note.find()
    .or([{_id : req.params.noteId},{versionOf : req.params.noteId}])
    .sort({ versionIndex: 'desc'})
    .then(notes => {
        if(notes.length > 1 || notes[0].versionIndex == 0){            
            res.send(notes);
        } else {
            Note.find()
                .or([{_id : notes[0].versionOf},{versionOf: notes[0].versionOf}])
                .sort({ versionIndex: 'desc'})
                .then(noteVersions => {
                    res.send(noteVersions);
                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Error occurred during retrieving notes."
                    });
                });
        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Error occurred during retrieving notes."
        });
    });
};

// Retrive full history of changes of all notes
exports.fullHistory = (req, res) => {
    Note.find()
    .then(notes => {
        res.send(notes);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Error occurred during retrieving notes."
        });
    });
};
