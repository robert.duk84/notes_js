const mongoose = require('mongoose');
let noteModel = require("../app/models/note.model")

let noteController = require("../app/controllers/note.controller")

let chai = require("chai");
let chaiHttp = require("chai-http");

const dbHandler = require('./db-test-handler');
let server = require("../server.js");

let baseUrl = "http://127.0.0.1:5000";

chai.should();
chai.use(chaiHttp);

//Connect to a new in-memory database before running any tests.
beforeAll(async () => {
    await dbHandler.connect();
});

//Clear all test data after every test.
afterEach(async () => {
    await dbHandler.clearDatabase();
});

// Remove and close the db and server.
afterAll(async () => {
await dbHandler.closeDatabase();
});

/**
 * TEST FOR NOTES_JS RESTFULL API APP
 */
describe('NOTES_JS RESTfull API test: ', () => {

    /**
    *   Test main Page
    */
    describe('TEST MAIN PAGE', () => {
        it('return main_page', done => {
            chai.request(baseUrl)
                .get("/")
                .end((err, res) => {
                    res.status.should.eql(200);
                    res.body.should.have.property('message').eql("Welcom to NOTES_JS application. ")
                done();
                });
            });
    });

    /**
    *   Test Create new Note
    */
    describe('ADD NEW NOTE', () => {
            it('create new note', (done) => {
                chai.request(baseUrl)
                .post("/notes")
                .send(correcrNoteDataForCreateNew)
                .end((err,res) => {
                    res.status.should.eql(200);
                    res.body.should.have.property('_id');
                    res.body.should.have.property('deleted').eql(false);
                    res.body.should.have.property('title').eql("Test Title");
                    res.body.should.have.property('content').eql("Test Content");
                    res.body.should.have.property('versionIndex').eql(0);
                    res.body.should.have.property('created');
                    res.body.should.have.property('modified');
                    res.body.should.have.property('__v').eql(0);
                done();
                });
            });

            it('unable to create note without title', (done) => {
                chai.request(baseUrl)
                .post("/notes")
                .send(creaNoteMissingTitleData)
                .end((err, res) => {
                    res.body.should.have.property('message').eql("Note validation failed: title: Path `title` is required.");
                done();
                });
            });

            it('unable to create note without content', (done) => {
                chai.request(baseUrl)
                .post("/notes")
                .send(creaNoteMissingContentData)
                .end((err, res) => {
                    res.body.should.have.property('message').eql("Note validation failed: content: Path `content` is required.");
                done();
                });
            });
    }); 

    /**
    *   Get all notes without deleted
    */
    describe('GET ALL NOTES WITHOUT DELETED ONES', () =>{
            it('return list of all notes without deleted ones', (done) => {
                chai.request(baseUrl)
                .get("/notes")
                .end((err, res) => {
                    res.status.should.eql(200);
                    res.body.should.be.a('array');
                done();
                })
            });
    });

    /**
    *   Get note by Id
    */
    describe('GET NOTE BY ID', () => {
        it('check by id - /notes/:id', (done) => {
            chai.request(baseUrl)
            .post("/notes")
            .send(correcrNoteDataForCreateNew)
            .end((err,res) => {
                let nodeId = res.body._id;
                chai.request(baseUrl)
                .get("/notes/" + nodeId)
                .end((err, res) => {
                    res.status.should.eql(200);
                    res.body.should.have.property('_id').eq(nodeId); 
                    res.body.should.have.property('deleted').eql(false);
                    res.body.should.have.property('title').eql("Test Title");
                    res.body.should.have.property('content').eql("Test Content");
                    res.body.should.have.property('versionIndex').eql(0);
                    res.body.should.have.property('created');
                    res.body.should.have.property('modified');
                    res.body.should.have.property('__v').eql(0);
                done();
                });
            });
        }); 

        it("get not existing note", (done) => {
            const noteId = 2;
            chai.request(baseUrl)
            .get("/notes/" + noteId)
            .end((err, res) => {
                res.status.should.eql(400);
            done();    
            });
        });
    });

    /**
    *   Update Note
    */
    describe('UPDATE NOTE BY ID', () => {
        it('update note with new title and content', (done) => {
            chai.request(baseUrl)
            .post("/notes")
            .send(correcrNoteDataForCreateNew)
            .end((err,res) => {
                let nodeId = res.body._id;
                chai.request(baseUrl)
                .put("/notes/" + nodeId)
                .send(correcrUpdateNoteData)
                .end((err, res) => {
                    res.status.should.eql(200); 
                    res.body.should.have.property('deleted').eql(false);
                    res.body.should.have.property('title').eql("Test Title Update");
                    res.body.should.have.property('content').eql("Test Content Update");
                    res.body.should.have.property('versionIndex').eql(1);
                    res.body.should.have.property('created');
                    res.body.should.have.property('modified');
                    res.body.should.have.property('__v').eql(0);
                done();
                });
            });
        }); 

        it("unable to udpate record with missing title", (done) => {
            chai.request(baseUrl)
            .post("/notes")
            .send(correcrNoteDataForCreateNew)
            .end((err,res) => {
                let nodeId = res.body._id;
                chai.request(baseUrl)
                .put("/notes/" + nodeId)
                .send(creaNoteMissingTitleData)
                .end((err, res) => {
                    res.body.should.have.property('message').eql("Note validation failed: title: Path `title` is required.");
                done(); 
                }); 
            });
        });

        it("unable to udpate record with missing content", (done) => {
            chai.request(baseUrl)
            .post("/notes")
            .send(correcrNoteDataForCreateNew)
            .end((err,res) => {
                let nodeId = res.body._id;
                chai.request(baseUrl)
                .put("/notes/" + nodeId)
                .send(creaNoteMissingContentData)
                .end((err, res) => {
                    res.body.should.have.property('message').eql("Note validation failed: content: Path `content` is required.");
                done(); 
                }); 
            });
        });
    });

    /**
    *   Delete Note
    */
    describe('DELETE NOTE BY ID', () => {
        it('delete note in existing task', (done) => {
            chai.request(baseUrl)
            .post("/notes")
            .send(correcrNoteDataForCreateNew)
            .end((err,res) => {
                let nodeId = res.body._id;
                chai.request(baseUrl)
                .delete("/notes/" + nodeId)
                .end((err, res) => {
                    res.body.should.have.property('message').eql("Note was deleted successfully!");
                done();
                });
            });
        }); 

        it("unable to udpate not existing record", (done) => {
            const noteId = 2;
            chai.request(baseUrl)
            .get("/notes/" + noteId)
            .end((err, res) => {
                res.status.should.eql(400);
            done();    
            });
        });
    });

});

const correcrNoteDataForCreateNew = {
    "title" : "Test Title",
    "content" : "Test Content"
};

const correcrUpdateNoteData = {
    "title" : "Test Title Update",
    "content" : "Test Content Update"
};

const creaNoteMissingTitleData = {
    content: "Test Content ",
};

const creaNoteMissingContentData = {
    title: "Test Title ",
};

